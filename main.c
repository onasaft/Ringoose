#include "mongoose.h"
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX_MEMS 100
#define MAX_SITE_LEN 80
#define MAX_NAME_LEN 63

char *proto = "https://";
char *port = "8989";
char *message = "A Webring Service in C.";

int num_mems = 0;
char urls[MAX_MEMS][MAX_SITE_LEN];

int find_order(char* url){
    for (int i = 0; i < num_mems; ++i) {
        if (strcmp(url, urls[i]) == 0) return i;
    }
    return -1;
}

void next(struct mg_connection *c, int* index){
    int i = *index;
    char header_val[MAX_NAME_LEN];
    sprintf(header_val, "Location: %s\r\n", urls[(i+1)%num_mems]);
    mg_http_reply(c, 302, header_val, "");
}

void prev(struct mg_connection *c, int* index){
    int i = *index;
    char header_val[MAX_NAME_LEN];
    if(i == 0) i = num_mems;
    sprintf(header_val, "Location: %s\r\n", urls[i-1]);
    mg_http_reply(c, 302, header_val, "");
}

void rndm(struct mg_connection *c){
    char header_val[MAX_NAME_LEN];
    srand(time(NULL));
    int r = rand() % num_mems;
    sprintf(header_val, "Location: %s\r\n", urls[r]);
    mg_http_reply(c, 302, header_val, "");
}

static void fn(struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
    if (ev == MG_EV_HTTP_MSG) {

        struct mg_http_message *hm = (struct mg_http_message *) ev_data;

        if (mg_http_match_uri(hm, "/")) {
            /* mg_http_serve_file(c, hm, "index.html", &opts); */
            mg_http_reply(c, 200, "", message);
        } else if (mg_http_match_uri(hm, "/rand")) {
            rndm(c);
        } else {
            struct mg_str *pmref = mg_http_get_header(hm, "Referer");
            struct mg_str mref;
            if (pmref == NULL) {
		rndm(c);
            } else {
                mref = *pmref;
                char *ref = malloc(mref.len + 1);
                snprintf(ref, mref.len + 1, "%s", mref.ptr);
                if(ref[mref.len-1] == '/' ) ref[mref.len-1] = 0;
                int order = find_order(ref);
                if(order == -1) {
		    rndm(c);
                } else if (mg_http_match_uri(hm, "/next")) {
                    next(c, &order);
                } else if (mg_http_match_uri(hm, "/prev")) {
                    prev(c, &order);
                } else {
                    mg_http_reply(c, 404, "", "That doesn't exist.");
                } free(ref);
            }
        }
    }
}

int main(int argc, char *argv[]) {

    FILE * fp;
    char * domain = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("list.txt", "r");
    if (fp == NULL) exit(EXIT_FAILURE);

    for (int i = 0; (read = getline(&domain, &len, fp)) != -1 ; ++i, ++num_mems){
        char url[9]; strcpy(url,  proto);
        domain[strlen(domain)-1] = 0;
        strcat(url, domain);
        strcpy(urls[i], url);
    }
    fclose(fp);

    char *str_port = malloc(20);
    sprintf(str_port, "%s0.0.0.0:%s", proto, port);

    struct mg_mgr mgr;
    mg_mgr_init(&mgr);                                        // Init manager
    mg_http_listen(&mgr, str_port , fn, &mgr);  // Setup listener
    for (;;) mg_mgr_poll(&mgr, 1000);                         // Event loop
    mg_mgr_free(&mgr);                                        // Cleanup
    free(str_port);
    return 0;
}
